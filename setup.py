#!/usr/bin/env python

from distutils.core import setup

setup(name='numpad',
      version='',
      description='Numerical prototyping in Python assisted by automatic differentiation ',
      author='Qiqi Wang',
      author_email='',
      url='https://github.com/qiqi/numpad',
      packages=['numpad'],
     )